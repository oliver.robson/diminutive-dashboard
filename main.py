import machine
import network
import os
import time
import sys

import config
from diminutive_dashboard import DiminutiveDashboard

ERROR_FN = "last_error.txt"

if (
    config.NETWORK_SSID == "YOUR WIFI SSID"
    and config.NETWORK_PASS == "YOUR WIFI PASSWORD"
):
    print("ERROR: Wifi configuration has not been set - bailing")
    sys.exit()

# Create network
network.WLAN(network.AP_IF).active(False)
sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)
sta_if.connect(config.NETWORK_SSID, config.NETWORK_PASS)
while not sta_if.isconnected():
    pass
print(f"Connected on ip {sta_if.ifconfig()[0]}")

dashboard = DiminutiveDashboard(config.SOURCES)

try:
    while True:
        run_time = time.time()

        print("Refreshing...")
        dashboard.refresh()

        sleep_time = config.REFRESH_PERIOD - (time.time() - run_time)
        print(f"Done refreshing, sleeping for {sleep_time} seconds")
        time.sleep(sleep_time)
except Exception as err:
    print("Error during refresh:", err)
    dashboard.set_error(str(err))

    if ERROR_FN in os.listdir():
        with open(ERROR_FN, "r") as error_file:
            last_error_time = int(error_file.readline())
    else:
        last_error_time = 0

    with open(ERROR_FN, "w") as error_file:
        error_file.write(f"{time.time()}\n{err}")

    # Only want to reset if it's been a reasonable length of time (and so likely just
    # cause by memory fragmentation and then a big network request). If we're raising
    # exceptions every refresh, then stop execution to leave the error message on the
    # display.
    time_since_reset = time.time() - last_error_time
    print(f"Last error occurred {time_since_reset} seconds ago")
    if time_since_reset < 0 or time_since_reset > config.REFRESH_PERIOD * 1.5:
        machine.reset()
