# Diminutive Dashboard

Diminutive Dashboard is your tiny portal into the internet. This MicroPython-based project allows you to easily retrieve and display information from public APIs on a small display.

![An M5Stack showing several statistics (the Diminutive Dashboard)](/docs/diminutive_demo.jpg)

The project has been specifically tested and setup with the [M5Stack](https://shop.m5stack.com/collections/stack-series/products/basic-core-iot-development-kit) in mind. While it should work with any MicroPython based device with an ILI9341 display driver being used, at the very least the pins will have to be configured.

## Configuration

You will need to create a custom `config.py` for running on your device. At the bare minimum this is because your Wi-Fi SSID and password aren't hardcoded into the repository (hopefully), but you will likely want to configure your dashboard sources to get the most value out of the dashboard (the default config shows the temperature and UV index for Melbourne, Australia).

- Run `create_config.py` (`python create_config.py`)
- Edit the newly created `config.py` to match your needs

`SOURCES` needs to be a list of `DashboardSource`s. Each `DashboardSource` can represent a single datapoint plucked from an API, or several datapoints from the same API request.

- `display_name` can either be a string, or a list of strings
- `api_url` must be a string that can be retrieved from the device with just a `GET` request
- Either `key` or `regex` must be specified
  - `key` is used where the API provides a JSON response. `key` should be a `.`-connected string of keys to navigate into the data and retrieve the requested piece of information. If a list is encountered, the first element will be used.
  - `regex` is used where the API response is not JSON and so a dict cannot easily be constructed from it. This should be a string (which will be put directly into `re.compile`).
  - If `display_name` is a list, `key` or `regex` (whichever is used) can either be a string (to use the same value for each display name, such as if the values have different transforms of the same data) or a list of the same length as `display_name`, with different data selectors to use for each of the names.
- `units` can be specified (either a single value to share between all values in the source or a list of different ones to use) to suffix the data value with a constant string.
- `transform` can be specified to apply a function to the data prior to displaying it. This can either be a single regex for all `display_name`s, or a different one for each. Pass it a function (or list of functions) which takes a single argument (the value of the API result once the key or regex has been applied) and returns a string. This could be used to convert fahrenheit to celcius, for example.
- `headers` can be specified as a dict of headers to send with the request, eg. `headers={"X-API-Key": "my_key"}`.

## Deployment

Deployment has been implemented for working with an M5Stack device on a Windows computer, and the below instructions are tailored towards that scenario. If you fall outside of these very narrow lines, you will need to adjust to match your environment.

### Prerequisites

- [mpremote](https://pypi.org/project/mpremote/)
- [mpy-cross](https://pypi.org/project/mpy-cross/)

### Exeuction

1. __NOTE:__ The following instructions will overwrite existing files on the device. Ensure anything you want to be saved is backed up prior to following the below instructions. Nobody except yourself is liable if you run any of these tools and lose code which you have not backed up, or for any damages caused (especially if you are running this on a non-M5Stack device while still using the M5Stack pin configurations).
2. Edit `deploy.bat` and replace `COM4` with the COM port that your device is connected on (can be found through the Device Manager).
3. Run `deploy.bat`
  - You should see a message saying `"Using port COMx for deployment"` (showing the port number you put in, not `x`), and a stream of messages as the code is copied to the device.
4. Your device should start running and show your dashboard!
  - If not, run `mpremote connect <YOUR_PORT>` and see what error messages are appearing.
