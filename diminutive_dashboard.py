import json
import gc
from machine import Pin, SPI
import re
import urequests

from ili934xnew import ILI9341
import m5stack
import tt32


class DashboardSource:
    def __init__(
        self,
        display_name,
        api_url,
        key=None,
        regex=None,
        units="",
        transform=None,
        headers={},
    ):
        if not key and not regex:
            raise ValueError(
                "Must specify either key (for json matches) or regex (for generic matches)"
            )
        if key and regex:
            raise ValueError("Cannot specify both key and regex")

        many = isinstance(display_name, list)
        if (
            many
            and (any(isinstance(x, list) for x in [key[0], regex]))
            and len(display_name) != len(key or regex)
        ):
            raise ValueError("Must provide as many keys / regexes as display names")

        self.display_names = display_name if many else [display_name]
        self.count = len(self.display_names)
        self.api_url = api_url

        self.keys = key if isinstance(key[0], list) else [key] * self.count
        regex = regex if isinstance(regex, list) else [regex] * self.count
        self.regexes = [re.compile(r) if r else None for r in regex]

        self.units = units if isinstance(units, list) else [units] * self.count
        self.transform = (
            transform if isinstance(transform, list) else [transform] * self.count
        )
        self.headers = headers

    @staticmethod
    def _deep_get(dict, keys):
        value = dict
        for key in keys:
            while isinstance(value, list):
                value = value[0]

            value = value.get(key, {})

        return value

    def get_values(self, data):
        values = []
        if all(self.keys):
            try:
                data = json.loads(data)
            except ValueError as err:
                print(f"Error parsing json response: {err}")
                return [None] * self.count

        for regex, key in zip(self.regexes, self.keys):
            if regex:
                match = regex.search(data)
                values.append(match.group(1) if match else None)
            elif key:
                value = self._deep_get(data, key)
                values.append(value)
            else:
                raise NotImplementedError("Can't get value if no key or regex provided")

        del data

        values = [
            self.transform[idx](v) if self.transform[idx] else v
            for idx, v in enumerate(values)
        ]

        return values


class DiminutiveDashboard:
    def __init__(self, sources):
        self.sources = sources

        self.power = Pin(m5stack.TFT_LED_PIN, Pin.OUT)
        self.power.value(1)

        self.spi = SPI(
            2,
            baudrate=40000000,
            miso=Pin(m5stack.TFT_MISO_PIN),
            mosi=Pin(m5stack.TFT_MOSI_PIN),
            sck=Pin(m5stack.TFT_CLK_PIN),
        )

        self.display = ILI9341(
            self.spi,
            cs=Pin(m5stack.TFT_CS_PIN),
            dc=Pin(m5stack.TFT_DC_PIN),
            rst=Pin(m5stack.TFT_RST_PIN),
            w=240,
            h=320,
            r=6,
        )
        self.display.set_font(tt32)
        self.display.erase()
        self.display.set_pos(0, 0)

        if not sources:
            error = "Must provide at least one source"
            self.display.print(f"Error: {error}")
            raise ValueError(error)
        if not all(isinstance(s, DashboardSource) for s in sources):
            error = "All sources must instances of DashboardSource"
            self.display.print(f"Error: {error}")
            raise ValueError(error)

        self.display.print("Loading...")

    def set_error(self, error):
        self.display.erase()
        self.display.set_pos(0, 0)
        self.display.print(error)

    def refresh(self):
        dashboards_strs = []
        for source in self.sources:
            gc.collect()
            print(f"Getting {source.display_names} ({gc.mem_free()})")
            vals = None
            req = urequests.get(source.api_url, headers=source.headers)
            if req.status_code != 200:
                print(
                    f"Request for {source.display_names} failed with return code {req.status_code}: {req.text}"
                )
            else:
                req = req.text
                vals = source.get_values(req)
                del req

            if vals is None:
                vals = ["x"] * len(source.display_names)

            for idx, val in enumerate(vals):
                dashboards_strs.append(
                    f"{source.display_names[idx]} : {val} {source.units[idx]}"
                )

        self.display.set_pos(0, 0)
        for dashboard_str in dashboards_strs:
            self.display.print(dashboard_str)
