@echo off

if not exist "config.py" (
    echo "Must create config.py (run create_config.py). Aborting deployment!"
    exit
)

set PORT=COM4
echo Using port %PORT% for deployment
python -m mpy_cross micropython-ili9341/ili934xnew.py
python -m mpy_cross micropython-ili9341/glcdfont.py
python -m mpy_cross micropython-ili9341/tt32.py
mpremote connect %PORT% cp ^
    main.py ^
    config.py ^
    diminutive_dashboard.py ^
    micropython-ili9341/glcdfont.mpy ^
    micropython-ili9341/ili934xnew.mpy ^
    micropython-ili9341/m5stack.py ^
    micropython-ili9341/tt32.mpy :

echo Done!
