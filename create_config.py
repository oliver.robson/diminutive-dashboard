import os

CONFIG_FN = "config.py"

if os.path.exists(CONFIG_FN):
    print("Config file already exists - if you wish to generate a new config file, delete or rename this first.")
    print("Aborting!")
    exit()

with open("config.py", "w") as config_file:
    config_file.write("""# Generated config file for Diminutive Dashboard - customise appropriately!
from diminutive_dashboard import DashboardSource

NETWORK_SSID = "YOUR WIFI SSID"
NETWORK_PASS = "YOUR WIFI PASSWORD"

REFRESH_PERIOD = 300  # How often to get data, in seconds

# List of DashboardSources containing info regarding each dashboard item
SOURCES = [
    DashboardSource(
        ["Date", "Time"],
        "http://worldtimeapi.org/api/ip",
        key="datetime",
        transform=[
            lambda v: v.split("T")[0],
            lambda v: ":".join(v.split("T")[1].split(":")[:2]),
        ],
    ),
    DashboardSource(
        ["Air Temp", "Feels Like"],
        # Uses API Pruner to make BOM API more manageable, see https://APIPruner.com
        "https://api.APIpruner.com/prune?url=http%3A%2F%2Freg.bom.gov.au%2Ffwo%2FIDV60901%2FIDV60901.94870.json&keys=observations&keys=data&keys=0",
        key=["data.air_temp", "data.apparent_t"],
        units="C",
    ),
    DashboardSource(
        "UV Index",
        "https://uvdata.arpansa.gov.au/xml/uvvalues.xml",
        regex=r'<location id="Melbourne">.+?<index>([0-9.]+)<',
    ),
]
"""
)
print(f"{CONFIG_FN} successfully generated! Now go customise it")
